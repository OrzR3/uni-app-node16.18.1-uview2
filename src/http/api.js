// // GET请求方式


import http from './request.js'

const api = {}

api.getAction = (url, params) => http.get(url, params)

api.postAction = (url, params) => http.post(url, params)

api.httpAction = (url, params, method) => http.httpAction(url, params, method)
// 获取字典
api.getDictItems = (data) => http.get('/sys/dict/getDictItems/'+ data, {})
// 登录
api.login = params => http.post('/sys/appletLogin', params)

api.register = params => http.post('/sys/user/register', params)

api.logout = () => http.post('/sys/logout', {})

api.randomImage = randTime => http.get('/sys/getCheckCode2/' + randTime, {});

api.postPhoneEmsCode = (keyCode, params) => http.get('/sys/user/postPhoneEmsCode/' + keyCode, params);

api.changeUserPassword = params =>  http.post('/sys/user/changeUserPassword', params);

/* 用户手机唯一校验 */
api.checkNewOnly = params => http.get('/sys/user/checkNewOnly', params);

export default api



