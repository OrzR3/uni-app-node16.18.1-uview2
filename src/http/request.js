// @ts-nocheck
import BASE_URL from './config'
//  统一给参数
const dataObj = (url, params) => {
  let options = params

  // #ifdef APP-PLUS
  // DOTO:暂时没处理过,只是放在这里
  let data = null; //业务数据
  let terminal = 1 //终端类型，web:0,app:1
  options = {
    ...params,
    data,
    sign,
    terminal
  }
  // #endif

  return options
}

const goLogin = () => {
  // uni.clearStorageSync();
  uni.reLaunch({
    url: '/pages/login/Login'
  }) //未授权，请重新登录(401)
}

// 请求错误处理
const checkError = (e, reject) => {
  // console.error("----接口错误----", e)
  if (e.data) {
    if (e.data.code) {
      switch (Number(e.data.code)) {
        case 401:
          goLogin()
          break;
      }
    }
    reject(e.data)
  } else {
    reject({
      'msg': '接口錯誤'
    })
  }
}

// 封装请求
const request = (method, url, options) => {
  let methods = '';
  let headers = {};
  switch (method) {
    case 'get':
      methods = 'GET'
      headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      }
      break;
    case 'post':
      methods = 'POST'
      headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
      break;
    case 'postByQuery':
      methods = 'POST'
      headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
      break;
    case 'put':
      methods = 'PUT'
      headers = {
        'Content-Type': 'application/json; charset=UTF-8'
      }
      break;
    case 'postForm':
      methods = 'POST'
      headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    case 'upload':
      methods = 'POST'
      headers = {
        'Content-Type': 'multipart/form-data; charset=UTF-8'
      }
      break;
  }
  if(uni.getStorageSync('Token')){
    headers['X-Access-Token'] = uni.getStorageSync('Token');
  }
  let obj = {},
    hideLoading = true,
    loadingText = '加载中...';
  if (options) { //如果有options
    if (options.hideLoading) {
      hideLoading = options.hideLoading
      delete options.hideLoading
    }
    if (options.loadingText) {
      loadingText = options.loadingText
      delete options.loadingText
    }
  }

  return new Promise((resolve, reject) => {
    !hideLoading && uni.showLoading({
      title: loadingText
    })
    uni.request({
      url: `${BASE_URL}${url}`,
      method: methods,
      data: dataObj(url, options),
      header: headers,
      success: res => { 
        if (res.statusCode == 200 || res.data.status == '1') {
          /* && (res.data.code == 200 || res.data.code == "00") */
          if (res.data) {
            resolve(res.data)
          } else {
            checkError(res, reject)
          }
        } else {
          if(res.statusCode == 500 && res.data.error == 'Internal Server Error'){
            msgInvalidLogin(res.data.message);
          }else{
            uni.showToast({
              title: res.message || '请求错误\r\n(' + res.data.message + ')',
              icon: 'none'
            })
          }
          reject({
            'msg': '请求錯誤（' + res.statusCode + ')'
          })
        }
      },
      fail: e => {
        checkError(e, reject)
      },
      complete: () => {
        setTimeout(function() {
          uni.hideLoading()
        }, 300);
      }
    })
  })
}


// 上传文件 封装请求
const uploadFile = (url, options) => {
  let tempData = options || {}
  uni.showLoading({
    title: "上传中..."
  })

  return new Promise((resolve, reject) => {
    uni.uploadFile({
      url: `${BASE_URL}${url}`,
      filePath: tempData.file,
      name: 'file',
      formData: tempData,
      success: res => {
        if (res.statusCode == 200) {
          let temp = JSON.parse(res.data)
          // console.log('temp',temp)
          if (temp.code == 200) {
            resolve(temp)
          } else {
            reject(temp)
            uni.showToast({
              title: temp.msg || '接口错误(' + temp.code + ')',
              icon: 'none'
            })
          }
        } else {
          uni.showToast({
            title: `未知错误(${res.statusCode})`,
            icon: 'none'
          })
        }
      },
      fail(e) {
        uni.showToast({
          title: '接口请求超时',
          icon: 'none'
        })
        reject(e.data)
      },
      complete: () => {
        uni.hideLoading()
      }
    });
  })
}


export default {
  get: (url, options) => {
    return request('get', url, options)
  },

  // JOSN格式
  post: (url, options) => {
    return request('post', url, options)
  },

  httpAction: (url, options, method) => {
    return request(method, url, options)
  },
  // form-data格式
  postForm: (url, options) => {
    return request('postForm', url, options)
  },

  // 上传
  upload: (url, options) => {
    return uploadFile(url, options)
  }
}

function msgInvalidLogin(msg){
  uni.showToast({
      title: msg,
      icon: 'none',
      duration: 3000,
  });
  let second = 3;
  const timer = setInterval(() => {
    second--;
    if (second) {
    } else {
      clearInterval(timer);
      // 手动清除 Toast
      uni.hideToast();
      uni.navigateTo({
          url: '/pages/login/login'
      })
    }
  }, 1000);
}