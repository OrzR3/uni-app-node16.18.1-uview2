import Vue from 'vue'
import App from './App'
import './uni.promisify.adaptor'
import '@/assets/scss/index.scss' // global css

import { togo } from '@/config/navTo.js'
Vue.prototype.$navTo = togo
import uView from "uview-ui";
Vue.use(uView);

import api from './http/api.js'
Vue.prototype.$api = api
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
