// @ts-nocheck
let baseUrl = "";
let socketUrl = "";
let uploadUrl = "";
if (process.env.NODE_ENV === 'development') {
	// 开发环境
	baseUrl = "http://192.168.0.145:9090/shzh";
} else if (process.env.NODE_ENV === 'production') {
	// 生产环境
	baseUrl = "http://61.187.56.147:8091/shzh";
}
uploadUrl = baseUrl + '/sys/common/upload';

const courtConfig = {
	//微信公众号APPID
	publicAppId: "",
	//请求接口
	baseUrl: baseUrl,
	//人脸验证的网址
  uploadUrl: uploadUrl,
	//webSocket地址
	socketUrl: socketUrl,
	//平台名称
	platformName: "科室助手",
	//项目logo
	logoUrl: "",
	//页面分享配置
	share: {
		title: '科室助手',
		// #ifdef MP-WEIXIN
		path: '/pages/home/home', //小程序分享路径
		// #endif
		// #ifdef H5 || APP-PLUS
		//公众号||APP分享
		desc: "科室助手", // 分享描述
		link: "", // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
		imgUrl: "", // 分享图标
		// #endif
	}
};
//手机号验证正则表达式
const phoneRegular = /^1\d{10}$/;
//邮箱验证正则表达式
const mailRegular = /^\w+@\w+(\.[a-zA-Z]{2,3}){1,2}$/;
//密码验证正则表达式
const passwordRegular = /^[a-zA-Z0-9]{4,10}$/;
export default Object.assign({
	phoneRegular,
	mailRegular,
	passwordRegular
}, courtConfig);